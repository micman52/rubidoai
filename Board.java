import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;

public class Board {
	private Piece[][] pieces;
	private Stack<HistoryItem> history;
	private Piece selectedPiece;
	private Game game;
	private CustomButton btnUndo, btnAI;
	private LinkedList<CanMove> resultsMoveLocations; 
	public Board(Game game) {
		this.pieces = new Piece[9][9];
		this.history = new Stack<HistoryItem>();
		
		this.game = game;
		this.btnUndo = new CustomButton("Undo", 480, 50, 80, 40);
		this.btnAI = new CustomButton("Al turn", 470, 92, 100, 40);
		this.resultsMoveLocations = new LinkedList<CanMove>();
	}
	
	public void initCross() {
		this.addSlot(-65, 0, 3, 0, false);
		this.addSlot(-20, 0, 4, 0, false);
		this.addSlot(25, 0, 5, 0, false);

		this.addSlot(-65, 45, 3, 1, false);
		this.addSlot(-20, 45, 4, 1, false);
		this.addSlot(25, 45, 5, 1, false);

		this.addSlot(-65, 90, 3, 2, false);
		this.addSlot(-20, 90, 4, 2, false);
		this.addSlot(25, 90, 5, 2, false);
		
	    this.addSlot(-200, 135, 0, 3, false);
	    this.addSlot(-155, 135, 1, 3, false);
	    this.addSlot(-110, 135, 2, 3, false);
	    this.addSlot(-65, 135, 3, 3, false);
	    this.addSlot(-20, 135, 4, 3, false);
	    this.addSlot(25, 135, 5, 3, false);
	    this.addSlot(70, 135, 6, 3, false);
	    this.addSlot(115, 135, 7, 3, false);
	    this.addSlot(160, 135, 8, 3, false);

	    this.addSlot(-200, 180, 0, 4, false);
	    this.addSlot(-155, 180, 1, 4, false);
	    this.addSlot(-110, 180, 2, 4, false);
	    this.addSlot(-65, 180, 3, 4, false);
	    this.addSlot(-20, 180, 4, 4, true);
	    this.addSlot(25, 180, 5, 4, false);
	    this.addSlot(70, 180, 6, 4, false);
	    this.addSlot(115, 180, 7, 4, false);
	    this.addSlot(160, 180, 8, 4, false);

	    this.addSlot(-200, 225, 0, 5, false);
	    this.addSlot(-155, 225, 1, 5, false);
	    this.addSlot(-110, 225, 2, 5, false);
	    this.addSlot(-65, 225, 3, 5, false);
	    this.addSlot(-20, 225, 4, 5, false);
	    this.addSlot(25, 225, 5, 5, false);
	    this.addSlot(70, 225, 6, 5, false);
	    this.addSlot(115, 225, 7, 5, false);
	    this.addSlot(160, 225, 8, 5, false);

	    this.addSlot(-65, 270, 3, 6, false);
	    this.addSlot(-20, 270, 4, 6, false);
	    this.addSlot(25, 270, 5, 6, false);

	    this.addSlot(-65, 315, 3, 7, false);
	    this.addSlot(-20, 315, 4, 7, false);
	    this.addSlot(25, 315, 5, 7, false);

	    this.addSlot(-65, 360, 3, 8, false);
	    this.addSlot(-20, 360, 4, 8, false);
	    this.addSlot(25, 360, 5, 8, false);
	    
	    this.game.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (btnUndo.contains(e.getPoint())) {
					undo();
					return;
				}

				if (btnAI.contains(e.getPoint())) {
					game.aiTurn();
					return;
				}
				
				for (int i = 0; i < 9; i++) {
					for (int j = 0; j < 9; j++) {
						Piece piece = pieces[i][j];
						if (piece != null) {
							if (piece.contains(e.getPoint())) {
								selectPiece(piece);
								return;
							}
						}
					}
				}
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				
			}
		});
	}
	
	public void render(Graphics2D g) {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (this.pieces[i][j] != null)
					this.pieces[i][j].render(g);
			}
		}
		
		this.btnAI.render(g);

		if (this.history.size() > 0)
			this.btnUndo.render(g);
	}

	// Creates a single game tile
	public void addSlot(int x, int y, int col, int row, Boolean isEmpty) {
		Piece piece = new Piece(x, y, 40, row, col, isEmpty);
		
		this.pieces[row][col] = piece;
	}

	// Called when a user wants to select a game tile
	public void selectPiece(Piece piece) {
		if (this.selectedPiece != null) {
			if (!piece.isEmpty()) {
				this.selectedPiece.deselect();
			}
			else {
				this.movePiece(piece);
				return;
			}
		}
		
		if (!piece.isEmpty()) {
			this.selectedPiece = piece;
			this.selectedPiece.select();
		}
		
		
		this.game.updateUI();
	}

	// Called when a user wants to move a ball
	public Piece movePiece(Piece piece) {
		Piece taken = this.validateMove(this.selectedPiece, piece);
		
		if (taken != null) {
			this.selectedPiece.removeBall();
            this.selectedPiece.deselect();
            
            this.history.push(new HistoryItem(this.selectedPiece, piece, taken));

            piece.addBall();

            this.selectedPiece = null;

            this.game.takeTurn();
            this.game.updateUI();

            return taken;
		}
		
		return null;
	}

	// Ensures that the move a user is making is valid
	// Returns the jumped piece if valid, and null if not valid
	public Piece validateMove(Piece src, Piece dest) {
		double angle = (Math.atan2(Math.abs(src.getY() - dest.getY()), Math.abs(src.getX() - dest.getX())) * 180 / Math.PI);
		int rowDiff = Math.abs(src.getRow() - dest.getRow());
		int colDiff = Math.abs(src.getCol() - dest.getCol());
		
	    if ((angle == 90 || angle == 0) && (rowDiff <= 2) && (colDiff <= 2)) {
	        if (src.getRow() >= dest.getRow() && (rowDiff == 1 || colDiff == 1))
	            return null;

	        Piece piece = this.pieces[(int) Math.ceil(((float)src.getRow() + dest.getRow()) / 2)][(int) Math.ceil(((float)src.getCol() + dest.getCol()) / 2)];

	        if (piece.isEmpty())
	            return null;

	        piece.removeBall();

	        return piece;
	    }
	    
	    return null;
	}

	// Undos the previous player's turn and resets score
	public void undo() {
		if (this.history.size() > 0) {
	        HistoryItem item = this.history.pop();

	        item.taken.addBall();
	        item.src.addBall();
	        item.dest.removeBall();

	        this.game.untakeTurn();
	        this.game.updateUI();
	    }
	}
	
	public Piece[][] getPieces() {
		return this.pieces;
	}
	
	class HistoryItem {
		private Piece src;
		private Piece dest;
		private Piece taken;
		
		public HistoryItem(Piece src, Piece dest, Piece taken) {
			this.src = src;
			this.dest = dest;
			this.taken = taken;
		}
	}

	/*AI FUNCTIONS
		these allow the AI to make/undo moves without updating UI*/

	// Called when a user wants to select a game tile
	public Piece aiselectPiece(Piece piece) {
		if (this.selectedPiece != null) {
			if (!piece.isEmpty()) {
				this.selectedPiece.deselect();
			}
			else {
				return this.aimovePiece(piece);
			}
		}
		
		if (!piece.isEmpty()) {
			this.selectedPiece = piece;
			this.selectedPiece.select();
		}
		return null;
	}

	// Called when a user wants to move a ball
	public Piece aimovePiece(Piece piece) {
		Piece taken = this.validateMove(this.selectedPiece, piece);
		
		if (taken != null) {
			this.selectedPiece.removeBall();
            this.selectedPiece.deselect();
            
            this.history.push(new HistoryItem(this.selectedPiece, piece, taken));

            piece.addBall();

            this.selectedPiece = null;

            return taken;
		}
		
		return null;
	}

	// Undos the previous player's turn and resets score
	public void aiundo() {
		if (this.history.size() > 0) {
	        HistoryItem item = this.history.pop();

	        item.taken.addBall();
	        item.src.addBall();
	        item.dest.removeBall();

	    }
	}
	public CombinedResults evalBoard(){
		CombinedResults combined = new CombinedResults();
		//Results totals = new Results();
		Results temp;
		resultsMoveLocations = new LinkedList<CanMove>();
		temp = (evalNine(4,1));
	    //totals.combine(temp);
	    temp.moveLocations=resultsMoveLocations;
		combined.addNorth(temp);
	    temp = (evalNine(4,7));
	    //totals.combine(temp);
	    resultsMoveLocations = new LinkedList<CanMove>();
	    temp.moveLocations=resultsMoveLocations;
		combined.addSouth(temp);
	    temp = (evalNine(4,4));
	    //totals.combine(temp);
	    resultsMoveLocations = new LinkedList<CanMove>();
	    temp.moveLocations=resultsMoveLocations;
		combined.addCenter(temp);
	    temp = (evalNine(1,4));
	    resultsMoveLocations = new LinkedList<CanMove>();
		temp.moveLocations=resultsMoveLocations;
		//totals.combine(temp);
	    combined.addWest(temp);
	    temp = (evalNine(7,4));
	    //totals.combine(temp);
	    resultsMoveLocations = new LinkedList<CanMove>();
		temp.moveLocations=resultsMoveLocations;
		combined.addEast(temp);
	   //Special Cases
	    boolean northSoft = false;
	    boolean southSoft = false;
	    boolean eastSoft = false;
	    boolean westSoft = false;
	    
	    	    
	   
			    
			  //Special case South  ooo
			    //                  ooo
			    //                  XXX
				boolean outerempty = false;
				boolean middleempty = false;
				boolean innerempty = false;
				boolean centerempty = false;
			    boolean cinnerempty = false;
			    
			    boolean outerfull = false;
				boolean middlefull = false;
				boolean innerfull = false;
				boolean innermiddlepiece = false;
				boolean centermiddlepiece = false;
			    boolean cinnermiddlepiece = false;
			    
			    
			    outerfull = (!pieces[3][8].isEmpty() && !pieces[4][8].isEmpty() &&  !pieces[5][8].isEmpty());
			    outerempty = (pieces[3][8].isEmpty() && pieces[4][8].isEmpty() &&  pieces[5][8].isEmpty());
			    middlefull = (!pieces[3][7].isEmpty() && !pieces[4][7].isEmpty() &&  !pieces[5][7].isEmpty());
			    middleempty = (pieces[3][7].isEmpty() && pieces[4][7].isEmpty() &&  pieces[5][7].isEmpty());
			    innerfull = (!pieces[3][6].isEmpty() && !pieces[4][6].isEmpty() &&  !pieces[5][6].isEmpty());
			    innerempty = (pieces[3][6].isEmpty() && pieces[4][6].isEmpty() &&  pieces[5][6].isEmpty());
			    centerempty = (pieces[3][5].isEmpty() && pieces[4][5].isEmpty() &&  pieces[5][5].isEmpty());
			    cinnerempty = (pieces[3][4].isEmpty() && pieces[4][4].isEmpty() &&  pieces[5][4].isEmpty());
				
			    innermiddlepiece = (pieces[3][6].isEmpty() && !pieces[4][6].isEmpty() &&  pieces[5][6].isEmpty());
				centermiddlepiece = (pieces[3][5].isEmpty() && !pieces[4][5].isEmpty() &&  pieces[5][5].isEmpty());
			    cinnermiddlepiece = (pieces[3][4].isEmpty() && !pieces[4][4].isEmpty() &&  pieces[5][4].isEmpty());
				
			    
			    if(outerfull && middleempty)
			    {
			    	if(innerempty)
			    		combined.south.addHard(3);
			    	else
			    		if(innermiddlepiece){
			    			combined.south.addHard(2);
			    			combined.south.addSoft(1);
			    		}
			    		else
			    			combined.south.addSoft(3);
		    	 }
			    else
			    	if(outerempty && middlefull && innerempty)
			    	{
				    	if(centerempty)
				    		combined.south.addHard(3);
				    	else
				    		if(centermiddlepiece){
				    			combined.south.addHard(2);
				    			combined.south.addSoft(1);
				    		}
				    		else
				    			combined.south.addSoft(3);
			    	 }
			    	else
			    		if(outerempty && middleempty && innerfull && centerempty)
			    		{
					    	if(cinnerempty)
					    		combined.south.addHard(3);
					    	else
					    		if(cinnermiddlepiece){
					    			combined.south.addHard(2);
					    			combined.south.addSoft(1);
					    		}
					    		else
					    			combined.south.addSoft(3);
				    	 }
			  
			  
			    outerempty = false;
				middleempty = false;
				innerempty = false;
				centerempty = false;
			    cinnerempty = false;
			    
			    outerfull = false;
				middlefull = false;
				innerfull = false;
				innermiddlepiece = false;
				centermiddlepiece = false;
			    cinnermiddlepiece = false;
			    
			    
			    outerfull = (!pieces[3][0].isEmpty() && !pieces[4][0].isEmpty() &&  !pieces[5][0].isEmpty());
			    outerempty = (pieces[3][0].isEmpty() && pieces[4][0].isEmpty() &&  pieces[5][0].isEmpty());
			    middlefull = (!pieces[3][1].isEmpty() && !pieces[4][1].isEmpty() &&  !pieces[5][1].isEmpty());
			    middleempty = (pieces[3][1].isEmpty() && pieces[4][1].isEmpty() &&  pieces[5][1].isEmpty());
			    innerfull = (!pieces[3][2].isEmpty() && !pieces[4][2].isEmpty() &&  !pieces[5][2].isEmpty());
			    innerempty = (pieces[3][2].isEmpty() && pieces[4][2].isEmpty() &&  pieces[5][2].isEmpty());
			    centerempty = (pieces[3][3].isEmpty() && pieces[4][3].isEmpty() &&  pieces[5][3].isEmpty());
			    cinnerempty = (pieces[3][4].isEmpty() && pieces[4][4].isEmpty() &&  pieces[5][4].isEmpty());
				
			    innermiddlepiece = (pieces[3][2].isEmpty() && !pieces[4][2].isEmpty() &&  pieces[5][2].isEmpty());
				centermiddlepiece = (pieces[3][3].isEmpty() && !pieces[4][3].isEmpty() &&  pieces[5][3].isEmpty());
			    cinnermiddlepiece = (pieces[3][4].isEmpty() && !pieces[4][4].isEmpty() &&  pieces[5][4].isEmpty());
				
			    
			    if(outerfull && middleempty)
			    {
			    	if(innerempty)
			    		combined.north.addHard(3);
			    	else
			    		if(innermiddlepiece){
			    			combined.north.addHard(2);
			    			combined.north.addSoft(1);
			    		}
			    		else
			    			combined.north.addSoft(3);
		    	 }
			    else
			    	if(outerempty && middlefull && innerempty)
			    	{
				    	if(centerempty)
				    		combined.north.addHard(3);
				    	else
				    		if(centermiddlepiece){
				    			combined.north.addHard(2);
				    			combined.north.addSoft(1);
				    		}
				    		else
				    			combined.north.addSoft(3);
			    	 }
			    	else
			    		if(outerempty && middleempty && innerfull && centerempty)
			    		{
					    	if(cinnerempty)
					    		combined.north.addHard(3);
					    	else
					    		if(cinnermiddlepiece){
					    			combined.north.addHard(2);
					    			combined.north.addSoft(1);
					    		}
					    		else
					    			combined.north.addSoft(3);
				    	 }
			  
			    
			    outerempty = false;
				middleempty = false;
				innerempty = false;
				centerempty = false;
			    cinnerempty = false;
			    
			    outerfull = false;
				middlefull = false;
				innerfull = false;
				innermiddlepiece = false;
				centermiddlepiece = false;
			    cinnermiddlepiece = false;
			    
			    
			    outerfull = (!pieces[0][3].isEmpty() && !pieces[0][4].isEmpty() &&  !pieces[0][5].isEmpty());
			    outerempty = (pieces[0][3].isEmpty() && pieces[0][4].isEmpty() &&  pieces[0][5].isEmpty());
			    middlefull = (!pieces[1][3].isEmpty() && !pieces[1][4].isEmpty() &&  !pieces[1][5].isEmpty());
			    middleempty = (pieces[1][3].isEmpty() && pieces[1][4].isEmpty() &&  pieces[1][5].isEmpty());
			    innerfull = (!pieces[2][3].isEmpty() && !pieces[2][4].isEmpty() &&  !pieces[2][5].isEmpty());
			    innerempty = (pieces[2][3].isEmpty() && pieces[2][4].isEmpty() &&  pieces[2][5].isEmpty());
			    centerempty = (pieces[3][3].isEmpty() && pieces[3][4].isEmpty() &&  pieces[3][5].isEmpty());
			    cinnerempty = (pieces[4][3].isEmpty() && pieces[4][4].isEmpty() &&  pieces[4][5].isEmpty());
				
			    innermiddlepiece = (pieces[2][3].isEmpty() && !pieces[2][4].isEmpty() &&  pieces[2][5].isEmpty());
				centermiddlepiece = (pieces[3][3].isEmpty() && !pieces[3][4].isEmpty() &&  pieces[3][5].isEmpty());
			    cinnermiddlepiece = (pieces[4][3].isEmpty() && !pieces[4][4].isEmpty() &&  pieces[4][5].isEmpty());
				
			    
			    if(outerfull && middleempty)
			    {
			    	if(innerempty)
			    		combined.west.addHard(3);
			    	else
			    		if(innermiddlepiece){
			    			combined.west.addHard(2);
			    			combined.west.addSoft(1);
			    		}
			    		else
			    			combined.west.addSoft(3);
		    	 }
			    else
			    	if(outerempty && middlefull && innerempty)
			    	{
				    	if(centerempty)
				    		combined.west.addHard(3);
				    	else
				    		if(centermiddlepiece){
				    			combined.west.addHard(2);
				    			combined.west.addSoft(1);
				    		}
				    		else
				    			combined.west.addSoft(3);
			    	 }
			    	else
			    		if(outerempty && middleempty && innerfull && centerempty)
			    		{
					    	if(cinnerempty)
					    		combined.west.addHard(3);
					    	else
					    		if(cinnermiddlepiece){
					    			combined.west.addHard(2);
					    			combined.west.addSoft(1);
					    		}
					    		else
					    			combined.west.addSoft(3);
				    	 }
			  //east SIDE TRIPLES
			    
			    outerempty = false;
				middleempty = false;
				innerempty = false;
				centerempty = false;
			    cinnerempty = false;
			    
			    outerfull = false;
				middlefull = false;
				innerfull = false;
				innermiddlepiece = false;
				centermiddlepiece = false;
			    cinnermiddlepiece = false;
			    
			    
			    outerfull = (!pieces[8][3].isEmpty() && !pieces[8][4].isEmpty() &&  !pieces[8][5].isEmpty());
			    outerempty = (pieces[8][3].isEmpty() && pieces[8][4].isEmpty() &&  pieces[8][5].isEmpty());
			    middlefull = (!pieces[7][3].isEmpty() && !pieces[7][4].isEmpty() &&  !pieces[7][5].isEmpty());
			    middleempty = (pieces[7][3].isEmpty() && pieces[7][4].isEmpty() &&  pieces[7][5].isEmpty());
			    innerfull = (!pieces[6][3].isEmpty() && !pieces[6][4].isEmpty() &&  !pieces[6][5].isEmpty());
			    innerempty = (pieces[6][3].isEmpty() && pieces[6][4].isEmpty() &&  pieces[6][5].isEmpty());
			    centerempty = (pieces[5][3].isEmpty() && pieces[5][4].isEmpty() &&  pieces[5][5].isEmpty());
			    cinnerempty = (pieces[4][3].isEmpty() && pieces[4][4].isEmpty() &&  pieces[4][5].isEmpty());
				
			    innermiddlepiece = (pieces[6][3].isEmpty() && !pieces[6][4].isEmpty() &&  pieces[6][5].isEmpty());
				centermiddlepiece = (pieces[5][3].isEmpty() && !pieces[5][4].isEmpty() &&  pieces[5][5].isEmpty());
			    cinnermiddlepiece = (pieces[4][3].isEmpty() && !pieces[4][4].isEmpty() &&  pieces[4][5].isEmpty());
				
			    
			    if(outerfull && middleempty)
			    {
			    	if(innerempty)
			    		combined.east.addHard(3);
			    	else
			    		if(innermiddlepiece){
			    			combined.east.addHard(2);
			    			combined.east.addSoft(1);
			    		}
			    		else
			    			combined.east.addSoft(3);
		    	 }
			    else
			    	if(outerempty && middlefull && innerempty)
			    	{
				    	if(centerempty)
				    		combined.east.addHard(3);
				    	else
				    		if(centermiddlepiece){
				    			combined.east.addHard(2);
				    			combined.east.addSoft(1);
				    		}
				    		else
				    			combined.east.addSoft(3);
			    	 }
			    	else
			    		if(outerempty && middleempty && innerfull && centerempty)
			    		{
					    	if(cinnerempty)
					    		combined.east.addHard(3);
					    	else
					    		if(cinnermiddlepiece){
					    			combined.east.addHard(2);
					    			combined.east.addSoft(1);
					    		}
					    		else
					    			combined.east.addSoft(3);
				    	 }
			  
			    
			    
			    
			    /*
			    //Special case North XXX
			    //                 ooo
			    //                 ooo
				   if(!pieces[3][0].isEmpty() && 
						   !pieces[4][0].isEmpty() &&
						   !pieces[5][0].isEmpty() &&
						   pieces[3][1].isEmpty() &&
						   pieces[4][1].isEmpty() &&
						   pieces[5][1].isEmpty())
					   northSoft = true;
				    if(northSoft)
				    	if ( pieces[3][2].isEmpty() &&
						   pieces[4][2].isEmpty() &&
						   pieces[5][2].isEmpty())
				    	{
				    		combined.north.addHard(3);
				    	}
				    	else
				    		combined.north.addSoft(3);
			    */
	    
	return combined;
		
	}
	public Results evalNine(int x,int y){
		int count = 0;
		int soft = 0;
		int hard = 0;
		int tempCount = 0;
		int piecesCanMove = 0;
		int numPiece = 0;
			for(int yy = -1; yy <= 1; yy++)
				for(int xx = -1; xx <= 1; xx++)
					if(pieces[x+xx][y+yy] != null )
						if(!pieces[x+xx][y+yy].isEmpty() ){
							tempCount=validateSingle((x+xx),(y+yy));
							if(checkSoft((x+xx),(y+yy))){
								if(checkHard((x+xx),(y+yy)))
									hard++;
								else
								soft++;
							}
							
							count+= tempCount;
							if(tempCount > 0)
								piecesCanMove++;
							tempCount = 0;
							numPiece++;
						}
		//System.out.println(x + " | " + y + " |M:" + count + "|P:" + numPiece+ "|Can:" + piecesCanMove );	
	return new Results(count,numPiece,piecesCanMove,soft,hard);
		
	}
	
	public int validateSingle(int x,int y){
		int count = 0;
		if((x-2) >= 0)
		count+= validateAINine(pieces[x][y],pieces[x-2][y]);
		if((x+2) <= 8)
		count+= validateAINine(pieces[x][y],pieces[x+2][y]);
		if((y-2) >= 0)
		count+= validateAINine(pieces[x][y],pieces[x][y-2]);
		if((y+2) <= 8)
		count+= validateAINine(pieces[x][y],pieces[x][y+2]);
		return count;
	}
	public boolean checkSoft(int x,int y){
		boolean test = true;
		if((x-1) >= 0 && true)
			if(!(pieces[x-1][y] == null || pieces[x-1][y].isEmpty()))
				test = false;
		if((x+1) <= 8 && true)
			if(!(pieces[x+1][y] == null || pieces[x+1][y].isEmpty()))
				test = false;
		if((y-1) >= 0 && true)
			if(!(pieces[x][y-1] == null || pieces[x][y-1].isEmpty()))
				test = false;
		if((y+1) <= 8 && true)
			if(!(pieces[x][y+1] == null || pieces[x][y+1].isEmpty()))
				test = false;
			
		return test;
	}
	public boolean checkHard(int x,int y){
		boolean test = true;
		if((x-3) >= 0){
			if((pieces[x-3][y] != null && !pieces[x-3][y].isEmpty()))
				if((pieces[x-2][y] != null && !pieces[x-2][y].isEmpty()))
				test = false;}
		
		if((x+3) <= 8 && test == true){
			if((pieces[x+3][y] != null && !pieces[x+3][y].isEmpty()))
				if((pieces[x+2][y] != null && !pieces[x+2][y].isEmpty()))
				test = false;}
		
		if((y-3) >= 0 && test == true){
			if((pieces[x][y-3] != null && !pieces[x][y-3].isEmpty()))
				if((pieces[x][y-2] != null && !pieces[x][y-2].isEmpty()))
				test = false;}
		
		if((y+3) <= 8 && test == true){
			if((pieces[x][y+3] != null && !pieces[x][y+3].isEmpty()))
				if((pieces[x][y+2] != null && !pieces[x][y+2].isEmpty()))
				test = false;}
		
		//Test Corners
		if(test==true)
			if(x-1 >=0 && y-1 >=0)
				if(pieces[x-1][y-1] != null && !pieces[x-1][y-1].isEmpty()){
					if(x-1 >=0 && y-2 >=0)
						if((pieces[x-1][y-2] != null && !pieces[x-1][y-2].isEmpty()))
							test = false;
					if(x-2 >=0 && y-1 >=0)
						if((pieces[x-2][y-1] != null && !pieces[x-2][y-1].isEmpty()))
							test = false;
				}
		if(test==true)
			if(x+1 <=8 && y+1 <=8)
		if(pieces[x+1][y+1] != null && !pieces[x+1][y+1].isEmpty()){
			
			if(x+1 <=8 && y+2 <=8)
			if((pieces[x+1][y+2] != null && !pieces[x+1][y+2].isEmpty()))
				test = false;
			if(x+2 <=8 && y+1 <=8)
			if( (pieces[x+2][y+1] != null && !pieces[x+2][y+1].isEmpty()))
				test = false;
			
		}
		if(test==true)
		if(x+1 <=8 && y-1 >=0)
		if(pieces[x+1][y-1] != null && !pieces[x+1][y-1].isEmpty()){
			if(x+1 <=8 && y-2 >=0)
			if((pieces[x+1][y-2] != null && !pieces[x+1][y-2].isEmpty()))
				test = false;
			if(x+2 <=8 && y-1 >=0)
			if( (pieces[x+2][y-1] != null && !pieces[x+2][y-1].isEmpty()))
				test = false;
		}
		if(test==true)
		if(x-1 >=0 && y+1 <=8)
		if(pieces[x-1][y+1] != null && !pieces[x-1][y+1].isEmpty()){
			if(x-1 >=0 && y+2 <=8)
			if((pieces[x-1][y+2] != null && !pieces[x-1][y+2].isEmpty()))
				test = false;
			if(x-2 >=0 && y+1 <=8)
			if( (pieces[x-2][y+1] != null && !pieces[x-2][y+1].isEmpty()))
			test = false;
		}
		
		
		
		
		
		
		
		return test;
	}
	
	// Returns the jumped piece if valid, and null if not valid
		public int validateAINine(Piece src, Piece dest) {
			if(dest == null || src == null)return 0;
			if(!dest.isEmpty())
				return 0;
			double angle = (Math.atan2(Math.abs(src.getY() - dest.getY()), Math.abs(src.getX() - dest.getX())) * 180 / Math.PI);
			int rowDiff = Math.abs(src.getRow() - dest.getRow());
			int colDiff = Math.abs(src.getCol() - dest.getCol());
			
		    if ((angle == 90 || angle == 0) && (rowDiff <= 2) && (colDiff <= 2)) {
		        if (src.getRow() >= dest.getRow() && (rowDiff == 1 || colDiff == 1))
		            return 0;

		        Piece piece = this.pieces[(int) Math.ceil(((float)src.getRow() + dest.getRow()) / 2)][(int) Math.ceil(((float)src.getCol() + dest.getCol()) / 2)];

		        if (piece.isEmpty())
		            return 0;

		        if(piece == null)
		        	return 0;
		        resultsMoveLocations.addLast(new CanMove(src.getRow(),src.getCol(),dest.getRow(),dest.getCol()));
		        return 1;
		    }
		    
		    return 0;
		}

	public LinkedList<CanMove> getMoves(){
		resultsMoveLocations = new LinkedList<CanMove>();
		
		evalNine(4,1);
	    evalNine(4,7);
	    evalNine(4,4);
	    evalNine(1,4);
	    evalNine(7,4);
	    /*LinkedList<CanMove> temp = new LinkedList<CanMove>();
	    //Random randomGenerator = new Random();
	    //int randomIndex = randomGenerator.nextInt(resultsMoveLocations.size());
	    int randomIndex =  (int)Math.random()*resultsMoveLocations.size();
	    if(resultsMoveLocations.size() == 0)
	    	return resultsMoveLocations;
	    for(int i=randomIndex;i <resultsMoveLocations.size()+randomIndex;i++)
	    	temp.addFirst(resultsMoveLocations.get(i%resultsMoveLocations.size()));*/
	    //System.out.println(temp.size());
	    return resultsMoveLocations;
	}
	
}
