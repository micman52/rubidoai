
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

public class CustomButton {
	private String text;
	private int x;
	private int y;
	private int width;
	private int height;
	private Rectangle hitBox;
	
	public CustomButton(String text, int x, int y, int width, int height) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.hitBox = new Rectangle(this.x, this.y, this.width, this.height);
	}
	
	public void render(Graphics2D g) {
		Font font = new Font("Arial", Font.BOLD, 20);

		g.setColor(Piece.SELECTED_COLOR);
		g.fillRect(this.x, this.y, this.width, this.height);
		
		g.setFont(font);
		g.setColor(Color.white);
		g.drawString(this.text, this.x + this.width / 6 + 3, this.y + (int)(this.height / 1.5));
	}
	
	public Boolean contains(Point2D point) {
		return this.hitBox.contains(point);
	}
}
