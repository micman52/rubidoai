import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;

public class Game extends Canvas {
	private BufferStrategy strategy;
	
	private Board board;
	private AI cpu;
	public Player player1;
	public Player player2;

	public Game(boolean aiFirst) {
		// create a frame to contain our game
		JFrame container = new JFrame("Chinese Checkers");

		// get hold the content of the frame and set up the
		// resolution of the game
		JPanel panel = (JPanel) container.getContentPane();
		panel.setPreferredSize(new Dimension(1024, 768));
		panel.setLayout(null);

		// setup our canvas size and put it into the content of the frame
		setBounds(0, 0, 1050, 800);
		panel.add(this);

		setIgnoreRepaint(true);

		// finally make the window visible
		container.pack();
		container.setResizable(false);
		container.setVisible(true);
		
		createBufferStrategy(2);
		this.strategy = getBufferStrategy();

		this.initUI(aiFirst);
	}

	public void aiTurn(){
	if(this.player2.isTurn()){
			cpu.makeMove();
		}
	}

	private void initUI(boolean aiFirst) {
		this.board = new Board(this);
		this.board.initCross();

		this.cpu = new AI(this);
		this.player1 = new Player(1, "Player1");
		this.player2 = new Player(2, "AI");
		
		if(aiFirst)
			this.player2.toggleTurn();
		else
			this.player1.toggleTurn();

		this.updateUI();
	}

	public void updateUI() {
		Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
		g.setColor(new Color(0xcccccc));
		g.fillRect(0,0,1050,800);
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		CombinedResults c = board.evalBoard();
		String temp = player1.getScore().toString();
		this.renderText(g, player1.getName(), player1.isTurn(), 20, 25);
		this.renderText(g, temp, player1.isTurn(), 20, 50);
		this.renderText(g, player2.getName(), player2.isTurn(), 20, 80);
		this.renderText(g, player2.getScore().toString(), player2.isTurn(), 20, 105);
		
		this.board.render(g);
	
		g.dispose();
		strategy.show();
	}

	public void takeTurn() {
		if (this.player1.isTurn()) {
            this.player1.incrementScore();
        }
        else if (this.player2.isTurn()) {
            this.player2.incrementScore();
        }

        this.player1.toggleTurn();
        this.player2.toggleTurn();
	}

	public void untakeTurn() {
		if (this.player1.isTurn()) {
            this.player2.decrementScore();
        }
        else if (this.player2.isTurn()) {
            this.player1.decrementScore();
        }

        this.player1.toggleTurn();
        this.player2.toggleTurn();
	}

	private void renderText(Graphics2D g, String name, Boolean bold, int x, int y) {
		Font font = new Font("Arial", bold ? Font.BOLD : Font.PLAIN, 25);
		
		g.setFont(font);
		g.setColor(Piece.SELECTED_COLOR);
		g.drawString(name, x, y);
	}

    /* PUBLIC METHODS - ALL METHODS LISTED HERE CAN BE ACCESSED ANYWHERE ELSE */
	
    // Returns the array of pieces on the board
	public Piece[][] getPieces() {
		return this.board.getPieces();
	}

    // Get player 1 object
	public Player getPlayer1() {
		return this.player1;
	}

    // Get player 2 object
	public Player getPlayer2() {
		return this.player2;
	}

	public Board getBoard(){
		return board;
	}

    // Moves a src piece to a destination piece - will return the jumped piece if valid otherwise will return null
	public Piece movePiece(Piece srcPiece, Piece destPiece) {
		this.board.selectPiece(srcPiece);
		Piece taken = this.board.movePiece(destPiece);
		
		srcPiece.deselect();
		
		this.updateUI();
		
		return taken;
	}

	// Moves a src piece to a destination piece - will return the jumped piece if valid otherwise will return null
	public Piece aimovePiece(Piece srcPiece, Piece destPiece) {
		this.board.aiselectPiece(srcPiece);
		Piece taken = this.board.aimovePiece(destPiece);
		
		srcPiece.deselect();
		
		return taken;
	}

	public static void main(String[] args) {
	    int button = JOptionPane.YES_NO_OPTION;
	    
	    int dialogResult = JOptionPane.showConfirmDialog (
	        null, "Will the AI go first?",
	        "Choose first player",
	        button
	    );
	    
        	Game game = new Game(dialogResult == JOptionPane.YES_OPTION);

	}
}
