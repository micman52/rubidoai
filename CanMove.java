
public class CanMove {
	public int fromX;
	public int fromY;
	public int toX;
	public int toY;
	//Stores src of move and dest of move.
	public CanMove(int fx, int fy, int tx, int ty){
		fromX = fx;
		fromY = fy;
		toX = tx;
		toY = ty;
	}
	//Not used atm.
	public boolean findPiece(int x, int y){
		if(fromX == x && fromY == y)
			return true;
		else
			return false;
	}
	//Checks to see if 2 pieces next to each other can jump each other.
	public boolean sideBySide(CanMove c){
		if(midX() == c.fromX && midY() == c.fromY &&
		   c.midX() == fromX && c.midY() == fromY)
			return true;
		else
			return false;
	}
	public int midX(){
		return (fromX+toX)/2;
	}
	public int midY(){
		return (fromY+toY)/2;
	}
	
	public String toString(){
		return "Loc:("+fromX +","+fromY+")->( "+toX +","+toY+")";
	}
}