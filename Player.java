

public class Player {
	private int id;
	private String name;
	private int score;
	private Boolean isTurn;
	
	public Player(int id, String name) {
		this.id = id;
		this.name = name;
		this.score = 0;
		this.isTurn = false;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Integer getScore() {
		return this.score;
	}
	
	public void incrementScore() {
		this.score++;
	}
	
	public void decrementScore() {
		this.score--;
	}
	
	public Boolean isTurn() {
		return this.isTurn;
	}
	
	public void toggleTurn() {
		this.isTurn = !this.isTurn;
	}
}
