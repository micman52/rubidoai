import java.util.LinkedList;

public class AI{
	private Game game;
	private DecTreeNode root;
    private static final int FIRST_DEPTH = 7,
                                SECOND_DEPTH = 11,
                                THIRD_DEPTH = 13,
                                FIRST_LIMIT = 19,
                                SECOND_LIMIT = 23,
                                LOSING_EVAL = -1000,
                                WINNING_EVAL = 1000;
	private int maxD;
	private int pruned = 0;
	private int checked = 0;
	Piece[] bestMove;
	public AI(Game g){
		game = g;
		root = new DecTreeNode();
		
		maxD = FIRST_DEPTH;
	}
	
	public void makeMove(){
		pruned = 0;
		checked = 0;
		CombinedResults cr1 = game.getBoard().evalBoard();
		int hardPnt = cr1.getHard()/2;
		int movesDone = game.player1.getScore() + game.player2.getScore();
		if(movesDone > (SECOND_LIMIT))
			maxD = THIRD_DEPTH;
		else if(movesDone > (FIRST_LIMIT))
			maxD = SECOND_DEPTH;
		
		System.out.println("Max Depth:"+maxD + " Moves Done:"+movesDone);
		//create tree from scratch
		root = new DecTreeNode();
		//make the tree
		makeDecTree(game.getBoard(), root, 0);
		
		//get the best move 
		System.out.println("Checked" + checked + "   Pruned:" + pruned);
		//make move if one exists
		if(bestMove != null)
			game.movePiece(bestMove[0],bestMove[1]);
	}

	//creates decision tree with depth maxD
	private int makeDecTree(Board b, DecTreeNode n, int d){
						
		Piece src, dest;
		int alpha = n.getAlpha(), beta = n.getBeta(), temp;
		//Gets all the moves from the current board.
		LinkedList<CanMove> moves = b.getMoves();
		
		//Get The Possible Moves.
		
		if(d >= maxD){
			CombinedResults cr = b.evalBoard();
			//get evaluation of leaf state
			//n.setLeafABValue(cr.eval());
			return (cr.eval());
		}						
		if(moves.size() == 0){
			if(d%2==0){
				n.setLeafABValue(LOSING_EVAL);
				return LOSING_EVAL;
			}
			else{
                n.setLeafABValue(WINNING_EVAL);
                return WINNING_EVAL;
			}
		} else {
            for(int p = 0; p < moves.size(); p++){
                CanMove cm = moves.get(p);
                src = b.getPieces()[cm.fromX][cm.fromY];
                //select src piece
                b.aiselectPiece(src);
                //get destination piece
                dest = b.getPieces()[cm.toX][cm.toY];
                //make sure destination is a actual piece and its empty
                //if src to dest is a valid move do it and continue
                if(b.aiselectPiece(dest) != null){
                    if (d % 2 == 0){
                        DecTreeNode newNode = new DecTreeNode(b.getPieces(), (d+1), alpha, beta, 0, src, dest);								
                        temp = makeDecTree(b, newNode, (d+1));
                        if (temp > alpha){
                            alpha = temp;
                            if (d == 0)
                                bestMove = newNode.getMove();
                        }
                        if (beta <= alpha){
                            pruned++;
                            p = 100;
                        }
                    } else {
                        DecTreeNode newNode = new DecTreeNode(b.getPieces(), (d+1), alpha, beta,0, src, dest);
                        temp = makeDecTree(b, newNode, (d+1));
                        if (beta > temp)
                        {
                            beta = temp;
                            if (d == 0)
                                bestMove = newNode.getMove();
                        }
                        if(beta <= alpha){
                            pruned++;
                            p = 100;
                        }
                    }
                    
                    //add only after exploring all possible moves from this new state									
                    checked++;

                    //undo this move and check for more at current node/state
                    b.aiundo();
                }
            }
        }
		if (d == 0)
		{
			System.out.println("Alpha is " + alpha);
		}
		if (d%2 == 0)
		{
			return alpha;
		}
		else
		{
			return beta;
		}

	}

	private int evalState(Board b){
		CombinedResults cr = b.evalBoard();
		return cr.eval();
	}
}
