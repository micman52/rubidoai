import java.util.LinkedList;


public class Results {
	private int numMoves;
	private int numPieces;
	private int numCanMove;
	public int soft;
	public int hard;
	public LinkedList<CanMove> moveLocations;
	public Results(){
		numMoves = 0;
		numPieces = 0;
		numCanMove = 0;
		soft =0;
		hard = 0;
		moveLocations = new LinkedList<CanMove>();
	}
	public Results(int numM, int numP, int numC, int numSoft, int numHard){
		numMoves = numM;
		numPieces = numP;
		numCanMove = numC;
		soft = numSoft;
		hard = numHard;
		moveLocations = new LinkedList<CanMove>();
	}
	
	public int getNumMoves() {
		return numMoves;
	}
	public void setNumMoves(int numMoves) {
		this.numMoves = numMoves;
	}
	public int getNumPieces() {
		return numPieces;
	}
	public void setNumPieces(int numPieces) {
		this.numPieces = numPieces;
	}
	public int getNumCanMove() {
		return numCanMove;
	}
	public void setNumCanMove(int numCanMove) {
		this.numCanMove = numCanMove;
	}
	public void addSoft(int s){
		soft += s;
	}
	public void addHard(int h){
		hard+= h;
	}
	public void combine(Results r){		
		numMoves += r.numMoves;
		numPieces += r.numPieces;
		numCanMove += r.numCanMove;
		soft += r.soft;
		hard += r.hard;
		if(r.moveLocations.size()!=0)
			for(int i = 0; i <= r.moveLocations.size()-1; i++){
			moveLocations.addLast(r.moveLocations.get(i));
		}
	}
	
	public void outputMoves(){
		System.out.println("Moves for current board:" + moveLocations.size());
		if(moveLocations.size()!=0)
		for(int i = 0; i <= moveLocations.size()-1; i++)
		System.out.println(moveLocations.get(i).toString());
	}
	public String toString(){
		return "Pieces:"+ numPieces + "|TotalMoves:"+numMoves+ "|#piecesCanMove:" + numCanMove + " |Soft:" + soft + " |Hard:" + hard;		
	}
	
}
