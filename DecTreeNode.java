import java.util.ArrayList;

public class DecTreeNode{
	private ArrayList<DecTreeNode> children;
	private Piece[][] state;
	public int alpha;
	public int beta;
	private int depth;
	private int value;
	private Piece[] move;

	public DecTreeNode(){
		children = new ArrayList<DecTreeNode>();
		depth = 0;
		value = 0;
		alpha = -5000;
		beta = 5000;
	}
	
	public DecTreeNode(Piece[][] s){
		children = new ArrayList<DecTreeNode>();
		state = s;
		depth = 0;
		value = 0;
		alpha = -5000;
		beta = 5000;
	}

	public DecTreeNode(Piece[][] s, int d){
		children = new ArrayList<DecTreeNode>();
		state = s;
		depth = 0;
		value = 0;
		alpha = -5000;
		beta = 5000;
	}

	public DecTreeNode(Piece[][] s, int d, int v){
		children = new ArrayList<DecTreeNode>();
		state = s;
		depth = d;
		value = v;
		alpha = -5000;
		beta = -5000;
	}

	public DecTreeNode(Piece[][] s, int d,int a,int b, int v, Piece src, Piece dest){
		//System.out.println("D:"+d);
		children = new ArrayList<DecTreeNode>();
		state = s;
		depth = d;
		value = v;
		move = new Piece[2];
		move[0] = src;
		move[1] = dest;
	
		alpha = a;
		beta = b;
	}

	public ArrayList<DecTreeNode> getChildren(){
		return children;
	}

	public DecTreeNode getChildAtIndex(int i){
		return children.get(i);
	}
	
	public boolean addChild(DecTreeNode child){
		
		if(depth%2==0)
		{	//System.out.println(depth + " | " + alpha + " AAA " + child.getValue());
			if(this.alpha <child.getValue())
				this.alpha = child.getValue();
		}
		if(depth%2==1)
		{
			//System.out.println(depth + " | " + beta + " BBB " + child.getValue());
			
			if(this.beta > child.getValue())
				this.beta = child.getValue();
			
		}
		//	System.out.print("A"+depth+"A"+child.getValue());
		
			
			
		return children.add(child);
	}
	
	public Piece[][] getState(){
		return state;
	}

	public void setState(Piece[][] s){
		state = s;
	}

	public int getAlpha() {
		return alpha;
	}

	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}

	public int getBeta() {
		return beta;
	}

	public void setBeta(int beta) {
		this.beta = beta;
	}

	public int getDepth(){
		return depth;
	}

	public void setDepth(int d){
		depth = d;
	}

	public int getValue(){
		return value;
	}

	public void checkAndSetAB(int v){
		if(depth %2 == 0){
			if(v > alpha){
	//			System.out.println("Alpha:"+alpha + "  V:" + v + " D:" + depth);
				
				alpha = v;
			
			}
		}
		else
		{
			if(v < beta){
		//		System.out.println("Beta:"+beta + "  V:" + v + " D:" + depth);
				
				beta = v;
			}
		}
		
	}
	
	public void setValue(int v){
		this.value = v;
	}
	public void setABValue(int v){
		
		this.value = v;
		if(depth %2 == 0){
			//System.out.println("Alpha:"+alpha + "  V:" + v + " D:" + depth);
			
			if(v > alpha){
				alpha = v;
			}
		}
		else
		{
			if(v < beta){
			//	System.out.println("Beta:"+beta + "  V:" + v + " D:" + depth);
				
				beta = v;
			}
		}
		
	}
	
	public void setLeafABValue(int v){
		//System.out.println("V: "+ v + " D:" + depth);
		this.value = v;	
	}
	
	
	public Piece[] getMove(){
	    return move;
	}
	
	public void setMove(Piece[] m){
	    move = m;
	}

	public void propagateValueFromChildren(){
		//if its MAX's turn
		
		if(depth % 2 == 0){
			int temp = getBestValue();
			if(temp > getValue())
				setABValue(temp);
			else value = temp;
			//if(value > alpha) alpha = value;
		}else{
			int temp = getWorstValue();
			if(temp < getValue())
				setABValue(temp);
			else value = temp;
			
			//if(value < beta) beta = value;
		}
	}

	public int getBestValue(){
	    int highestValue = -5000, temp;
	    //go through all first level children (possible moves at current state) and get reference to node with highest value
	   // System.out.print("Depth:" + depth + " | ");
	    for(DecTreeNode n: children){
			if((temp = n.getValue()) > highestValue)
			    highestValue = temp;
	    //System.out.print(temp+"|");
	    }
	    //if (depth%2==0)System.out.println("|" + highestValue + " A:" + alpha + "  B:" + beta);
	    return highestValue;
	}

	public int getWorstValue(){
	    int worstValue = 5000, temp;
	    //go through all first level children (possible moves at current state) and get reference to node with highest value
	    //System.out.print("Depth:" + depth + " | ");
	    for(DecTreeNode n: children){
			if((temp = n.getValue()) < worstValue)
			    worstValue = temp;
			//System.out.print(temp+"#");
	    }
	    //if (depth%2 == 1)System.out.println("#" + worstValue + " A:" + alpha + "  B:" + beta);
	    return worstValue;
	}

	public Piece[] getBestMove(){
	    int highestValue = -5000;
	    int temp;
	    //currently this will hold the last highest valued node in the event of ties
	    DecTreeNode best = null;
	    //go through all first level children (possible moves at current state) and get reference to node with highest value
	    for(DecTreeNode n: children){
			if((temp = n.getValue()) > highestValue){
			    highestValue = temp;
			    best = n;
			}
	    }
	    System.out.println("Alpha:" + alpha + " Beta:" + beta);
	    if(best != null)
	    	return best.getMove();

	    //if no children
	    return null;
	}

	public Piece[] getWorstMove(){
		int lowestValue = 5000;
	    int temp;
	    //currently this will hold the last lowest valued node in the event of ties
	    DecTreeNode worst = null;
	    //go through all first level children (possible moves at current state) and get reference to node with highest value
	    for(DecTreeNode n: children){
			if((temp = n.getValue()) < lowestValue){
			    
				lowestValue = temp;
			    worst = n;
			}
	    }
	    if(worst != null)
	    	return worst.getMove();

	    //if no children
	    return null;
	}
}
