

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

public class Piece {
	private int x;
	private int y;
	private int row;
	private int col;
	private int radius;
	private Boolean isSelected;
	private Boolean isEmpty;
	public final static Color SELECTED_COLOR = new Color(0x4B78DB);
	
	private Shape circle;
	private Shape ball;
	
	public Piece(int x, int y, int radius, int row, int col, Boolean isEmpty) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.row = row;
		this.col = col;
		this.isEmpty = isEmpty;
		this.isSelected = false;
		
		this.circle = new Ellipse2D.Double(this.x + 524, this.y + 172, this.radius, this.radius);
		this.ball = new Ellipse2D.Double(this.x + 529, this.y + 177, 30, 30);
	}
	
	public void render(Graphics2D g) {
		if (this.isSelected) {
			g.setColor(SELECTED_COLOR);
		}
		else {
			g.setColor(Color.white);
		}
		
		g.fill(this.circle);
		
		if (!this.isEmpty) {
			g.setColor(Color.black);
			g.fill(this.ball);
		}
	}
	
	public boolean contains(Point2D point) {
		return this.circle.contains(point);
	}
	
	public void select() {
		this.isSelected = true;
	}
	
	public void deselect() {
		this.isSelected = false;
	}
	
	public void addBall() {
		this.isEmpty = false;
	}
	
	public void removeBall() {
		this.isEmpty = true;
		this.isSelected = false;
	}
	
	public Boolean isEmpty() {
		return this.isEmpty;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getRow() {
		return this.row;
	}
	
	public int getCol() {
		return this.col;
	}
}
