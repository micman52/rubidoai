import java.util.LinkedList;


public class CombinedResults {
	public Results north;
	public Results south;
	public Results west;
	public Results east;
	public Results center;
	public Results totals;
	
	public CombinedResults(){
		
		north = new Results();
		south = new Results();
		center= new Results();
		west  = new Results();
		east  = new Results();
		totals = new Results();
	}
	
	public void addNorth(Results r){
		north = r;
	}
	
	public void addSouth(Results r){
		south = r;
	}
	public void addCenter(Results r){
		center = r;
			}
	public void addWest(Results r){
		west = r;
	}
	public void addEast(Results r){
		east = r;
	}
	
	public LinkedList<CanMove> combine(){
		Results totals = new Results();
		totals.combine(north);
		totals.combine(south);
		totals.combine(east);
		totals.combine(west);
		totals.combine(center);
		
	return totals.moveLocations;	
	}
	
	public int eval(){
		int results = 0;
		float quadrants = 0;
		Results totals = new Results();
		totals.combine(north);
		totals.combine(south);
		totals.combine(east);
		totals.combine(west);
		totals.combine(center);
		//totals.outputMoves();
		int deltaMoves = totals.getNumMoves() - totals.getNumCanMove();	
		//SBS = oxxo
		int sideBySide = 0;
		//quadrants = (float)(north.getNumPieces() + south.getNumPieces() + east.getNumPieces() + west.getNumPieces())/(totals.getNumPieces());
		//Find SBS pieces
		if(totals.moveLocations.size()!=0)
			for(int i = 0; i < totals.moveLocations.size(); i++){
			for(int j = i+1; j < totals.moveLocations.size(); j++)
				if(totals.moveLocations.get(i).sideBySide(totals.moveLocations.get(j)))
					sideBySide++;
			}
		int numdiv2 = totals.getNumPieces()/2;
		
		if(totals.getNumCanMove()==0)
		{results=1000-(numdiv2*4);}
		else
		//Num can move is 1 and the number of piece minus the hard points = 2 (1 real move left)
		if(totals.getNumCanMove()==1)
			results=-300;
		else
		if(totals.getNumCanMove()%2 == 1 && sideBySide == 0 && (totals.getNumPieces() - totals.hard)%2==1)
			{results=-200+numdiv2;}
		else 
			if(totals.getNumCanMove()%2==0 && sideBySide == 1 && (totals.getNumPieces() - totals.hard)%2==0)
				{results=-100+numdiv2;}
			else
				if(totals.getNumCanMove()%2 == 1 && sideBySide == 0 && (totals.getNumPieces() - totals.hard-totals.soft)%2==1)
					{results=-75+numdiv2;}
				else
					if(totals.getNumCanMove()%2 == 0 && sideBySide == 1 && (totals.getNumPieces() - totals.hard-totals.soft)%2==0)
						{results=-50+numdiv2;}
					else
					
						
							if(totals.getNumCanMove()%2==0 && sideBySide == 0 && (totals.getNumPieces() - totals.hard)%2 == 0)
								{results=150-numdiv2;}
							else
								if(totals.getNumCanMove()%2==1 && sideBySide == 1 && (totals.getNumPieces() - totals.hard)%2 == 1)
									{results=100-numdiv2;}
								else
									if(totals.getNumCanMove()%2==0 && sideBySide == 0 && (totals.getNumPieces() - totals.hard - totals.soft)%2 == 0)
										{results=75-numdiv2;}
									else
										if(totals.getNumCanMove()%2==1 && sideBySide == 1 && (totals.getNumPieces() - totals.hard - totals.soft)%2 == 1)
											{results=50-numdiv2;}
							results+= (totals.getNumPieces() - totals.getNumCanMove())*2;
							if(sideBySide > 0) 
								results-=sideBySide * 5;
							//Hard points should equal opposite of player number
							results += 4*totals.hard-deltaMoves;
							//results -= totals.soft*10;
							if(results ==0 && (totals.getNumCanMove()) % 2 == 0)
								results = 2;
							if(results == 0) results =1;
							
		return results;
	}
	public int getHard(){
		Results totals = new Results();
		totals.combine(north);
		totals.combine(south);
		totals.combine(east);
		totals.combine(west);
		totals.combine(center);
		return totals.hard;
		
		
		
	}
	public int getMoves(){
		Results totals = new Results();
		totals.combine(north);
		totals.combine(south);
		totals.combine(east);
		totals.combine(west);
		totals.combine(center);
		return totals.getNumMoves();
		
		
		
	}
	public String toString(){
		Results totals = new Results();
		totals.combine(north);
		totals.combine(south);
		totals.combine(east);
		totals.combine(west);
		totals.combine(center);
		return totals.toString();
		
		
		
	}
}
